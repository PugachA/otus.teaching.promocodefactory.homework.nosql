﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly IMongoCollection<T> _mongoCollection;

        public MongoRepository(IMongoDatabase mongoDatabase)
        {
            _mongoCollection = mongoDatabase.GetCollection<T>(typeof(T).Name.ToLower());
        }

        public async Task AddAsync(T entity)
        {
            await _mongoCollection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _mongoCollection.DeleteOneAsync<T>(e => e.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return (await _mongoCollection.FindAsync(e => true)).ToList();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return (await _mongoCollection.FindAsync(e => e.Id == id)).FirstOrDefault();
        }

        public async Task<T> GetFirstWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return (await _mongoCollection.FindAsync(e => predicate.Compile().Invoke(e))).FirstOrDefault();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return (await _mongoCollection.FindAsync(e => ids.Contains(e.Id))).ToList();
        }

        public async Task<IEnumerable<T>> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return (await _mongoCollection.FindAsync(e => predicate.Compile().Invoke(e))).ToList();
        }

        public async Task UpdateAsync(T entity)
        {
            await _mongoCollection.ReplaceOneAsync(e => e.Id == entity.Id, entity);
        }
    }
}
