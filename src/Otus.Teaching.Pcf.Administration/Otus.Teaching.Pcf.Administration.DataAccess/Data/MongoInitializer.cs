﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoInitializer : IDbInitializer
    {
        private readonly IMongoClient _mongoClient;
        private readonly IMongoDatabase _mongoDatabase;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public MongoInitializer(
            IMongoClient mongoClient,
            IMongoDatabase mongoDatabase,
            IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository)
        {
            _mongoClient = mongoClient;
            _mongoDatabase = mongoDatabase;
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        public void InitializeDb()
        {
            _mongoClient.DropDatabase(_mongoDatabase.DatabaseNamespace.DatabaseName);

            foreach (var t in FakeDataFactory.Employees)
                _employeeRepository.AddAsync(t).GetAwaiter().GetResult();

            foreach (var t in FakeDataFactory.Roles)
                _roleRepository.AddAsync(t).GetAwaiter().GetResult();
        }
    }
}
